# DkML JNR

This project is a set of patches to https://github.com/jnr/jnr-ffi/blob/master/docs/README.md,
especially https://github.com/jnr/jnr-ffi and https://github.com/jnr/jffi.

* Changes to JFFI to get Android working (almost): [JFFI](docs/JFFI.md)

Pending:

* JFFI uses dynamic bytecode creation which
  [Android does not support](https://developer.android.com/training/articles/perf-jni.html#unsupported-featuresbackwards-compatibility).
  In particular `jnr.ffi.provider.jffi.AsmClassLoader#defineClass` will fail with:

  ```text
  java.lang.UnsupportedOperationException: can't load this type of class file
  ```

## Building

### Android Studio

1. Open `View > Tool Windows > Gradle`.
2. Double-click on the `com.diskuv.dkml.jnr > Tasks > publishing > publishToMavenLocal` task.

### Command Line

In a Unix shell or in Windows PowerShell, run:

```shell
./dk dksdk.java.jdk.download NO_SYSTEM_PATH
./dk dksdk.gradle.download ALL NO_SYSTEM_PATH
./dk dksdk.android.ndk.download NO_SYSTEM_PATH
./dk dksdk.android.gradle.configure OVERWRITE
```

Then:

```shell
./dk dksdk.gradle.run ARGS publishToMavenLocal
```

This project typically will _not_ need to be upgraded when JNR is upgraded.

A command line that uses a different version is:

```shell
# [jffiRef] is the git tag (or git commit id)
./dk dksdk.gradle.run ARGS publishToMavenLocal -P jffiRef=jffi-1.3.11
```
