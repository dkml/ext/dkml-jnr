# JFFI

## Problem Summary

Upstream JNR does not support Android. That lack of support is due to these issues:

1. The Android Gradle Plugin (AGP 7.4) does not load `jffi-X.Y.Z-native.jar`
   (ex. https://repo1.maven.org/maven2/com/github/jnr/jffi/1.2.23/) as a dependency when you
   declare a Gradle dependency `implementation 'com.github.jnr:jnr-ffi:2.2.14'`.
   Normally the `<scope>runtime</scope> <classifier>native</classifier>` at
   https://github.com/jnr/jnr-ffi/blob/0deaac0c731974cdcb9b64ce1a6f1d1ebf7e565a/pom.xml#L67-L73
   would be enough. However, it seems that AGP skips over
   `<classifier>native</classifier>` dependencies.
2. AGP strips every `jni/<architecture>/<library>.so` from the `.apk`, although it leaves all other
   (`.dll`, `.a`, etc.) libraries alone. For example,
   `app/build/intermediates/apk/debug/app-debug.apk` could contain only:

   ```
   jni
   ├── Darwin
   │   └── libjffi-1.2.jnilib
   ├── ppc64-AIX
   │   └── libjffi-1.2.a
   ├── x86_64-Windows
   │   └── jffi-1.2.dll
   ├── ppc-AIX
   │   └── libjffi-1.2.a
   └── i386-Windows
       └── jffi-1.2.dll
   ```

   It is not surprising that the .so libraries are removed because AGP uses that `jni` directory
   for its own purposes.

   Example: https://cs.android.com/android-studio/platform/tools/base/+/mirror-goog-studio-main:build-system/gradle-core/src/main/java/com/android/build/gradle/tasks/PackageAndroidArtifact.java;l=970-1008;drc=a3695ab10bcb8fb04e4f5502916d803edee1faae
3. (Serious) The `jffi-X.Y.Z-native.jar` does platform detection rather than ABI detection.
   That means that Android gets detected as `x86_64-Linux`, `aarch64-Linux`, etc. However the
   bundled native shared libraries for `*-Linux` are compiled against a some version of
   GNU libc (`glibc`). We cannot *safely* mix GNU libc linked libraries with musl linked libraries
   or Bionic Android C libraries. Said a different way, the `*-Linux` libraries are for the
   GNU/Linux "library" ABI only.

   > Advanced: GNU has some documentation about what they mean by "library" ABI at
   > https://gcc.gnu.org/onlinedocs/libstdc++/manual/abi.html. Part of the ABI is the glibc
   > version.

We produce a single module that solves the above issues. The subsections below tell how it is done.

## Solution - bundling shared libraries into Android

The first and second issues requires that we include `.so` files in a way that AGP supports. That
means one of the following methods:

* bundling the `com.github.jnr:jffi:1.3.11:native` as a conventional AAR bundle which has
  Android ABI-specific shared libraries in the `jni/<android-abi-name>/xxx.so` path of the bundle.
  But the AAR bundle would
  require a change to `com.kenai.jffi.internal.StubLoader.load()` to stop searching the classpath.
  Not interested in a fork; the JNR team would have to make those changes.
* placing the prebuilt native libraries into the `src/main/jniLibs` folder as described at
  https://developer.android.com/studio/projects/gradle-external-native-builds#jniLibs. Sadly that
  folder does not support a custom hierarchy (ex. `jni/Darwin/libjffi-1.2.jnilib`).
* placing the prebuilt native libraries as Android assets

We tried the Android assets method. That works but is unnecessarily complicated. For now we
do the first technique, but we let `com.kenai.jffi.internal.StubLoader.load()` fail. It is the
responsibility of the developer to hack at its internals to pretend it actually worked, and do a
`System.loadLibrary("jffi-1.2")` to finish up the hack:

```java
public class Example {
    public static SOME_CLASS loadLibraryFromAar(String libraryName) {
        /* Load the JNR native library from the com.diskuv.dkml.jnr:jffi-native-android AAR.
           This is not supported by JNR today so StubLoader's static initializer will complain
           but we'll patch it up. */
        String jffiLib = String.format(Locale.ENGLISH, "jffi-%d.%d",
                com.kenai.jffi.internal.StubLoader.VERSION_MAJOR,
                com.kenai.jffi.internal.StubLoader.VERSION_MINOR);
        System.loadLibrary(jffiLib);
    
        try {
            /* [patch] StubLoader.loaded = true; */
            Field loadedField = com.kenai.jffi.internal.StubLoader.class.getDeclaredField("loaded");
            loadedField.setAccessible(true);
            loadedField.set(com.kenai.jffi.internal.StubLoader.class, true);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    
        return LibraryLoader.create(SOME_CLASS.class).load(libraryName);
    }
}
```

## Solution - Android ABI libraries

The third issue requires that we build the `.so` files using an Android toolchain. There is
an Android module `jffi-native-android` that does just that. The module produces an AAR bundle
which, unlike a traditional AAR, bundles the shared libraries into the Android assets folder.
The module also has convenience Java methods to load the shared libraries from within the AAR.
