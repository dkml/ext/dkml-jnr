# libffi custom port

Adapted from
https://github.com/microsoft/vcpkg/tree/d132d128b48b1fd9418a873e8694cd18ae69ae06/ports/libffi
and from DkSDK.

The following major exceptions to vcpkg libffi apply:

1. Adds support for cross-compiling on macOS. For example, when on a ARM64 host and cross-compiling to
   a x86_64 target, the original libffi would simply use CMake host variables like ${CMAKE_SYSTEM_PROCESSOR}
   to figure out which source files to include.

   The end result would be a failure like the following when linking using `dune build -x darwin_x86_64`:

        Undefined symbols for architecture x86_64:
        "_ffi_call", referenced from:
            _ctypes_call in libctypes_foreign_stubs.a(ffi_call_stubs.o)
        "_ffi_prep_cif_machdep", referenced from:
            _ffi_prep_cif_core in libffi.a(prep_cif.c.o)
        "_ffi_prep_cif_machdep_var", referenced from:
            _ffi_prep_cif_core in libffi.a(prep_cif.c.o)
        "_ffi_prep_closure_loc", referenced from:
            _ctypes_make_function_pointer in libctypes_foreign_stubs.a(ffi_call_stubs.o)
            _ffi_prep_closure in libffi.a(prep_cif.c.o)
        ld: symbol(s) not found for architecture x86_64

   because while building libffi in vcpkg we would get:

        [1/9] /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc -DFFI_BUILDING -DFFI_DEBUG -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg/include -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/include -g -arch x86_64 -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX12.3.sdk -mmacosx-version-min=10.15 -MD -MT CMakeFiles/libffi.dir/src/aarch64/sysv.S.o -MF CMakeFiles/libffi.dir/src/aarch64/sysv.S.o.d -o CMakeFiles/libffi.dir/src/aarch64/sysv.S.o -c /tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/src/aarch64/sysv.S
        [2/9] /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc -DFFI_BUILDING -DFFI_DEBUG -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg/include -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/include -fPIC -mmacosx-version-min=10.15 -g -arch x86_64 -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX12.3.sdk -mmacosx-version-min=10.15 -MD -MT CMakeFiles/libffi.dir/src/tramp.c.o -MF CMakeFiles/libffi.dir/src/tramp.c.o.d -o CMakeFiles/libffi.dir/src/tramp.c.o -c /tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/src/tramp.c
        [3/9] /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc -DFFI_BUILDING -DFFI_DEBUG -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg/include -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/include -fPIC -mmacosx-version-min=10.15 -g -arch x86_64 -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX12.3.sdk -mmacosx-version-min=10.15 -MD -MT CMakeFiles/libffi.dir/src/aarch64/ffi.c.o -MF CMakeFiles/libffi.dir/src/aarch64/ffi.c.o.d -o CMakeFiles/libffi.dir/src/aarch64/ffi.c.o -c /tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/src/aarch64/ffi.c
        [4/9] /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc -DFFI_BUILDING -DFFI_DEBUG -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg/include -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/include -fPIC -mmacosx-version-min=10.15 -g -arch x86_64 -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX12.3.sdk -mmacosx-version-min=10.15 -MD -MT CMakeFiles/libffi.dir/src/types.c.o -MF CMakeFiles/libffi.dir/src/types.c.o.d -o CMakeFiles/libffi.dir/src/types.c.o -c /tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/src/types.c
        [5/9] /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc -DFFI_BUILDING -DFFI_DEBUG -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg/include -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/include -fPIC -mmacosx-version-min=10.15 -g -arch x86_64 -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX12.3.sdk -mmacosx-version-min=10.15 -MD -MT CMakeFiles/libffi.dir/src/debug.c.o -MF CMakeFiles/libffi.dir/src/debug.c.o.d -o CMakeFiles/libffi.dir/src/debug.c.o -c /tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/src/debug.c
        [6/9] /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc -DFFI_BUILDING -DFFI_DEBUG -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg/include -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/include -fPIC -mmacosx-version-min=10.15 -g -arch x86_64 -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX12.3.sdk -mmacosx-version-min=10.15 -MD -MT CMakeFiles/libffi.dir/src/closures.c.o -MF CMakeFiles/libffi.dir/src/closures.c.o.d -o CMakeFiles/libffi.dir/src/closures.c.o -c /tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/src/closures.c
        [7/9] /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/cc -DFFI_BUILDING -DFFI_DEBUG -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg/include -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/x64-osx-dbg -I/tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/include -fPIC -mmacosx-version-min=10.15 -g -arch x86_64 -isysroot /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX12.3.sdk -mmacosx-version-min=10.15 -MD -MT CMakeFiles/libffi.dir/src/prep_cif.c.o -MF CMakeFiles/libffi.dir/src/prep_cif.c.o.d -o CMakeFiles/libffi.dir/src/prep_cif.c.o -c /tmp/dckbuild/darwin_x86_64/vcpkg/buildtrees/libffi/src/v3.4.2-43b890eb15.clean/src/prep_cif.c
        [8/9] : && /private/tmp/dckbuild/darwin_x86_64/vcpkg/downloads/tools/cmake-3.21.1-osx/cmake-3.21.1-macos-universal/CMake.app/Contents/bin/cmake -E rm -f libffi.a && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ar qc libffi.a  CMakeFiles/libffi.dir/src/closures.c.o CMakeFiles/libffi.dir/src/prep_cif.c.o CMakeFiles/libffi.dir/src/types.c.o CMakeFiles/libffi.dir/src/tramp.c.o CMakeFiles/libffi.dir/src/aarch64/ffi.c.o CMakeFiles/libffi.dir/src/aarch64/sysv.S.o CMakeFiles/libffi.dir/src/debug.c.o && /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib libffi.a && /private/tmp/dckbuild/darwin_x86_64/vcpkg/downloads/tools/cmake-3.21.1-osx/cmake-3.21.1-macos-universal/CMake.app/Contents/bin/cmake -E touch libffi.a && :
        /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib: file: libffi.a(ffi.c.o) has no symbols
        /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib: file: libffi.a(sysv.S.o) has no symbols
        /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib: file: libffi.a(ffi.c.o) has no symbols
        /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/ranlib: file: libffi.a(sysv.S.o) has no symbols

   All of those `libffi.a(MM.X.o) has no symbols` are because the wrong `src/NN/MM.X` source file was picked.
   In the above `src/aarch64/ffi.c.o` is chosen even though the clang compiler is x86_64.

   Only `CMakeLists.txt` needed to be updated.
